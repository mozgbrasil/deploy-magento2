#!/bin/bash

# Copyright © 2016-2019 Mozg. All rights reserved.
# See LICENSE.txt for license details.

setVars () {
echo -e "${ONYELLOW} setVars ${RESETCOLOR}"

WICH_7ZA=`which 7za`
WICH_TAR=`which tar`
WICH_MYSQL=`which mysql`
GIT=`which git` > /dev/null
PHP_BIN=`which php`
OS=`uname -s`
REV=`uname -r`
MACH=`uname -m`
WHOAMI=$(whoami &2>/dev/null)
FOLDER_UP="$(cd ../; pwd)"
BASE_PATH_USER=~
FOLDER_CACHE=$BASE_PATH_USER'/dados/softwares'

# Define text styles
echo $SHELL
echo
echo $TERM
BOLD=''
NORMAL=''
[ -z ${TERM} ] || {
  #BOLD=`tput bold` # Error Heroku, tput: No value for $TERM and no -T specified
  #NORMAL=`tput sgr0`
  BOLD=$(tput bold)
  NORMAL=$(tput sgr0)
}

# Reset
RESETCOLOR='\e[0m'       # Text Reset

# Regular Colors
BLACK='\e[0;30m'        # Black
RED='\e[0;31m'          # Red
GREEN='\e[0;32m'        # Green
YELLOW='\e[0;33m'       # Yellow
BLUE='\e[0;34m'         # Blue
PURPLE='\e[0;35m'       # Purple
CYAN='\e[0;36m'         # Cyan
WHITE='\e[0;37m'        # White

# Background
ONBLACK='\e[40m'       # Black
ONRED='\e[41m'         # Red
ONGREEN='\e[42m'       # Green
ONYELLOW='\e[43m'      # Yellow
ONBLUE='\e[44m'        # Blue
ONPURPLE='\e[45m'      # Purple
ONCYAN='\e[46m'        # Cyan
ONWHITE='\e[47m'       # White

# Nice defaults
NOW_2_FILE=$(date +%Y-%m-%d_%H-%M-%S)
DATE_EN_US=$(date '+%Y-%m-%d %H:%M:%S')
DATE_PT_BR=$(date '+%d/%m/%Y %H:%M:%S')

}

setVars

#

function_before () {
echo
}

function_after () {
echo -e "${ONYELLOW}}${RESETCOLOR}"
echo
}

show_vars () {

function_before
echo -e "${ONYELLOW} show_vars () { ${RESETCOLOR}"

echo -e "${ONYELLOW} date ${RESETCOLOR}"

echo $(date +%Y-%m-%d_%H-%M-%S)

echo -e "${ONYELLOW} pwd ${RESETCOLOR}"

pwd

#df -h

#du -hsx ./* | sort -rh | head -10

#echo -e "${ONYELLOW} whoami - print effective userid ${RESETCOLOR}"

#whoami

##echo -e "${ONYELLOW} printenv - print all or part of environment ${RESETCOLOR}"

#printenv

#echo -e "${ONYELLOW} ps - report a snapshot of the current processes ${RESETCOLOR}"

#ps aux

#echo -e "${ONYELLOW} will list all the commands you could run. ${RESETCOLOR}"

#compgen -A function -abck

function_after

}

mysql_show_tables () {

function_before
echo -e "${ONYELLOW} mysql_show_tables () { ${RESETCOLOR}"

MYSQL_SHOW_TABLES=`mysql -h "${MAGE_DB_HOST}" -P "${MAGE_DB_PORT}" -u "${MAGE_DB_USER}" -p"${MAGE_DB_PASS}" "${MAGE_DB_NAME}" -N -e "SHOW TABLES"`

echo -e "${ONPURPLE} - ${RESETCOLOR}"

#echo $MYSQL_SHOW_TABLES

function_after

}

mysql_select_admin_user () {

function_before
echo -e "${ONYELLOW} mysql_select_admin_user () { ${RESETCOLOR}"

MYSQL_SELECT_ADMIN_USER=`mysql -h "${MAGE_DB_HOST}" -P "${MAGE_DB_PORT}" -u "${MAGE_DB_USER}" -p"${MAGE_DB_PASS}" "${MAGE_DB_NAME}" -N -e "SELECT * FROM admin_user"`

echo -e "${ONPURPLE} - ${RESETCOLOR}"

#echo $MYSQL_SELECT_ADMIN_USER

function_after

}

run_n98_magerun2 () {

#function_before
echo -e "${ONYELLOW} run_n98_magerun2 () { ${RESETCOLOR}"

echo -e "${ONYELLOW} check n98-magerun2 ${RESETCOLOR}"

pwd

timeProg=`which n98-magerun2`

[[ "$(command -v n98-magerun2)" ]] || { echo "n98-magerun2 is not installed" 1>&2 ; }
[[ -f "./n98-magerun2.phar" ]] || { echo "n98-magerun2 local installed" 1>&2 ; }

if [ ! -f "./n98-magerun2.phar" ]; then # -z String, True if string is empty.
  echo -e "${ONYELLOW} n98-magerun2 ${RESETCOLOR}"
  wget https://files.magerun.net/n98-magerun2.phar
  chmod +x ./n98-magerun2.phar
fi

php bin/magento --version
./n98-magerun2.phar --version
./n98-magerun2.phar sys:check

#./n98-magerun2.phar --root-dir=magento local-config:generate "$MAGE_DB_HOST:$MAGE_DB_PORT" "$MAGE_DB_USER" "$MAGE_DB_PASS" "$MAGE_DB_NAME" "files" "admin" "secret" -vvv

#function_after
echo -e "${ONYELLOW} } ${RESETCOLOR}"

}

export -f run_n98_magerun2

background_commands () {

echo -e "${ONYELLOW} background_commands () { ${RESETCOLOR}"



echo -e "${ONYELLOW} } ${RESETCOLOR}"

}

release_host () {

#function_before
echo -e "${ONYELLOW} release_host () { ${RESETCOLOR}"

echo -e "${ONYELLOW} path ${RESETCOLOR}"

pwd
cd magento
pwd

echo -e "${ONYELLOW} ls -lah app/etc ${RESETCOLOR}"

ls -lah app/etc

echo -e "${ONYELLOW} CREATE: app/etc/env.php ${RESETCOLOR}"

php -d memory_limit=-1 bin/magento setup:config:set --backend-frontname "admin" --db-host "${MAGE_DB_HOST}:${MAGE_DB_PORT}" --db-name "${MAGE_DB_NAME}" --db-user "${MAGE_DB_NAME}" --db-engine "innodb" --db-password "${MAGE_DB_PASS}" --db-prefix "" --db-model "mysql4" --session-save "files"

echo -e "${ONYELLOW} ls -lah app/etc ${RESETCOLOR}"

ls -lah app/etc

echo -e "${ONYELLOW} CREATE: app/etc/config.php ${RESETCOLOR}"

php -d memory_limit=-1 bin/magento module:enable --all --clear-static-content --quiet

echo -e "${ONYELLOW} ls -lah app/etc ${RESETCOLOR}"

ls -lah app/etc

echo -e "${ONYELLOW} WARNING: Please run the 'setup:di:compile' command to generate classes. ${RESETCOLOR}"

php -d memory_limit=-1 bin/magento setup:di:compile

echo -e "${ONYELLOW} after_install  ${RESETCOLOR}"

#nohup bash -c after_install &
after_install

echo -e "${ONYELLOW} path ${RESETCOLOR}"

pwd
cd ..
pwd

#function_after
echo -e "${ONYELLOW} } ${RESETCOLOR}"

}

after_install () {

#function_before
echo -e "${ONYELLOW} after_install () { ${RESETCOLOR}"

echo -e "${ONYELLOW} - ${RESETCOLOR}"

pwd
cd magento
pwd

#php -d memory_limit=-1 bin/magento maintenance:enable
#php -d memory_limit=-1 bin/magento setup:upgrade
#php -d memory_limit=-1 bin/magento maintenance:disable
#php -d memory_limit=-1 bin/magento setup:di:compile
#php -d memory_limit=-1 bin/magento setup:static-content:deploy -f
#php -d memory_limit=-1 bin/magento cache:clean
#php -d memory_limit=-1 bin/magento cache:flush

#nohup bash -c "php -d memory_limit=-1 bin/magento cache:flush 2>&1 &" && sleep 2
#nohup bash -c "php -d memory_limit=-1 bin/magento setup:static-content:deploy -f 2>&1 &" && sleep 2

echo -e "\e[1;33m --( du -hsx )-- \e[0m"
du -hsx ./* | sort -rh | head -10

echo -e "\e[1;33m --( php --version )-- \e[0m"
php --version

echo -e "\e[1;33m --( composer --version )-- \e[0m"
composer --version

echo -e "\e[1;33m --( composer diagnose )-- \e[0m"
composer diagnose

echo -e "\e[1;33m --( composer show )-- \e[0m"
composer show -s -vvv

echo -e "\e[1;33m --( pwd )-- \e[0m"
pwd

echo -e "\e[1;33m --( bin/magento )-- \e[0m"
php -d memory_limit=-1 bin/magento

echo -e "\e[1;33m --( bin/magento )-- \e[0m"
php -d memory_limit=-1 bin/magento setup:db:status

echo -e "\e[1;33m --( bin/magento cache:disable )-- \e[0m"
php -d memory_limit=-1 bin/magento cache:disable

#echo -e "\e[1;33m --( bin/magento cache:clean )-- \e[0m"
#php -d memory_limit=-1 bin/magento cache:clean

#echo -e "\e[1;33m --( bin/magento cache:flush )-- \e[0m"
#php -d memory_limit=-1 bin/magento cache:flush

#echo -e "\e[1;33m --( bin/magento cache:status )-- \e[0m"
#php -d memory_limit=-1 bin/magento cache:status

echo -e "\e[1;33m --( bin/magento deploy:mode:show )-- \e[0m"
php -d memory_limit=-1 bin/magento deploy:mode:show

echo -e "\e[1;33m --( bin/magento deploy:mode:set developer )-- \e[0m"
php -d memory_limit=-1 bin/magento deploy:mode:set developer

echo -e "\e[1;33m --( bin/magento indexer:reindex )-- \e[0m"
php -d memory_limit=-1 bin/magento indexer:reindex

echo -e "\e[1;33m --( bin/magento indexer:status )-- \e[0m"
php -d memory_limit=-1 bin/magento indexer:status

echo -e "\e[1;33m --( bin/magento maintenance:status )-- \e[0m"
php -d memory_limit=-1 bin/magento maintenance:status

#echo -e "\e[1;33m --( bin/magento module:status )-- \e[0m"
#php -d memory_limit=-1 bin/magento module:status

echo -e "\e[1;33m --( bin/magento setup:upgrade )-- \e[0m"
#php -d memory_limit=-1 bin/magento setup:upgrade #  --keep-generated  Schema creation/updates: heroku[router]: at=error code=H12 desc="Request timeout"

#echo -e "\e[1;33m --( index.php )-- \e[0m"
#php index.php # Necessário comentar devido ao erro "Exception #0 (InvalidArgumentException): DOCUMENT_ROOT variable is unavailable." Este script é usado apenas como um ponto de entrada do site, não é usado para comandos CLI

echo -e "${ONYELLOW} - ${RESETCOLOR}"

pwd
cd ..
pwd

#function_after

}

magento_install () {

function_before
echo -e "${ONYELLOW} magento_install () { ${RESETCOLOR}"

echo -e "${ONYELLOW} path ${RESETCOLOR}"

pwd
cd magento
pwd

echo -e "${ONYELLOW} Aplicando permissões ${RESETCOLOR}"

find var generated vendor pub/static pub/media app/etc -type f -exec chmod g+w {} +
find var generated vendor pub/static pub/media app/etc -type d -exec chmod g+ws {} +

if [ -f .env ] ; then # if file exits
  echo -e "${RED} .env ${RESETCOLOR}"
  # Ubuntu
  sudo chown -R :www-data . 
  sudo chmod u+x bin/magento
fi

echo -e "${ONYELLOW} install.php ${RESETCOLOR}"

php bin/magento setup:install \
--base-url="$MAGE_URL" \
--db-host="${MAGE_DB_HOST}:${MAGE_DB_PORT}" \
--db-name="${MAGE_DB_NAME}" \
--db-user="${MAGE_DB_USER}" \
--db-password="${MAGE_DB_PASS}" \
--backend-frontname="admin" \
--admin-firstname="Marcio" \
--admin-lastname="Amorim" \
--admin-email="mailer@mozg.com.br" \
--admin-user="admin" \
--admin-password="123456a" \
--language="pt_BR" \
--currency="BRL" \
--timezone="America/Sao_Paulo" \
--use-rewrites="1"

echo -e "${ONYELLOW} magento/index.php ${RESETCOLOR}"

php index.php

echo -e "${ONYELLOW} after_install ${RESETCOLOR}"

after_install

echo -e "${ONYELLOW} https://magenticians.com/why-magento-2-is-slow/ ${RESETCOLOR}"

php bin/magento config:set catalog/frontend/flat_catalog_product 1
# php bin/magento config:set dev/js/enable_js_bundling 1 # Production
php bin/magento config:set dev/js/merge_files 1
php bin/magento config:set dev/js/minify_files 1
php bin/magento config:set dev/css/merge_css_files 1
php bin/magento config:set dev/css/minify_files 1
php bin/magento config:set dev/static/sign 1

echo -e "${ONYELLOW} redis-pg-cache ${RESETCOLOR}"

# https://devdocs.magento.com/guides/v2.3/config-guide/redis/redis-pg-cache.html

php bin/magento setup:config:set --cache-backend=redis --cache-backend-redis-server=127.0.0.1 --cache-backend-redis-db=0

php bin/magento setup:config:set --page-cache=redis --page-cache-redis-server=127.0.0.1 --page-cache-redis-db=1

echo -e "${ONYELLOW} redis-session ${RESETCOLOR}"

# https://devdocs.magento.com/guides/v2.3/config-guide/redis/config-redis.html

# https://devdocs.magento.com/guides/v2.3/config-guide/redis/redis-session.html#result

php bin/magento setup:config:set --session-save=redis --session-save-redis-host=localhost --session-save-redis-log-level=3 --session-save-redis-db=2

echo -e "${ONYELLOW} path ${RESETCOLOR}"

pwd
cd ..
pwd

echo -e "${ONYELLOW} - ${RESETCOLOR}"

function_after

}

release () {

function_before
echo -e "${ONYELLOW} release () { ${RESETCOLOR}"


function_after

}

post_update_cmd () { # post-update-cmd: occurs after the update command has been executed, or after the install command has been executed without a lock file present.
# Na heroku o Mysql ainda não foi instalado nesse ponto

function_before
echo -e "${ONYELLOW} post_update_cmd () { ${RESETCOLOR}"

echo -e "${ONYELLOW} - ${RESETCOLOR}"

if [ -d magento/vendor/prasathmani/tinyfilemanager ]; then
    echo -e "${ONYELLOW} prasathmani/tinyfilemanager ${RESETCOLOR}"
    cp -fr magento/vendor/prasathmani/tinyfilemanager/ .
fi

echo -e "${ONYELLOW} - ${RESETCOLOR}"

du -hsx ./* | sort -rh | head -10
du -hsx  magento/vendor/* | sort -rh | head -10

echo -e "${ONYELLOW} - ${RESETCOLOR}"

show_vars
profile
after_install

echo -e "${ONYELLOW} - ${RESETCOLOR}"

function_after

}

post_install_cmd () { # post-install-cmd: occurs after the install command has been executed with a lock file present.

function_before
echo -e "${ONYELLOW} post_install_cmd () { ${RESETCOLOR}"

post_update_cmd

function_after

}

postdeploy () { # postdeploy command. Use this to run any one-time setup tasks that make the app, and any databases, ready and useful for testing.

function_before
echo -e "${ONYELLOW} postdeploy () { ${RESETCOLOR}"

post_update_cmd # post-update-cmd: occurs after the update command has been executed, or after the install command has been executed without a lock file present.

function_after

}

profile () { # Heroku, During startup, the container starts a bash shell that runs any code in $HOME/.profile before executing the dyno’s command. You can put bash code in this file to manipulate the initial environment, at runtime, for all dyno types in your app.

function_before
echo -e "${ONYELLOW} profile () { ${RESETCOLOR}"

pwd

echo -e "${ONYELLOW} check mysql ${RESETCOLOR}"

if type mysql >/dev/null 2>&1; then

  echo -e "${ONGREEN} mysql installed ${RESETCOLOR}"

  if [ -f ".env" ] ; then # if file exits, only local
    magento_install
  fi

else
  echo -e "${ONRED} mysql not installed ${RESETCOLOR}"
fi

echo -e "${ONYELLOW} release_host ${RESETCOLOR}"

if [ ! -f ".env" ] ; then # if file not exits, only heroku ...
  if [ ! -f "magento/app/etc/env.php" ] ; then # if file not exits
    release_host
  fi
fi

echo -e "${ONYELLOW} - ${RESETCOLOR}"

function_after

}

#

echo -e "${ONYELLOW} .env loading in the shell ${RESETCOLOR}"

dotenv () {
  set -a
  [ -f .env ] && . .env
  set +a
}

dotenv

echo -e "${ONYELLOW} env MAGE_ ${RESETCOLOR}"

env | grep ^MAGE_

#

METHOD=${1}

if [ "$METHOD" ]; then
  $METHOD
else
  echo -e "${ONRED} abort () { ${RESETCOLOR}"
fi

# 

#curl --request POST "https://fleep.io/hook/OLuIRi0JRt2yv5OQisX6tg" --data $1
